# RMC Motor HUB

This board provides a "bridge" between RMC and a motor system (drivers, motors, encoders).


## Libraries 
In order to use this project one should download LSF's KiCad library (https://gitlab.com/librespacefoundation/lsf-kicad-lib.git) 
and configure LSF_KICAD_LIB as an environment variable in KiCad (Preferences > Configure Paths).


